Codegears::Application.routes.draw do
  scope '/', defaults: { format: 'json' } do
    root :to => 'main#index'

    match 'plans/:data_center_id' => 'plans#index',        as: :plans
    match 'data-centers'          => 'data_centers#index', as: :data_centers

    scope 'instances' do
      resources :app_instances, path: 'app'
      resources :db_instances,  path: 'db'
    end
  end
end
