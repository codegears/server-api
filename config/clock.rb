require_relative  "environment"
require 'clockwork'

Clockwork.every(5.minutes, 'plans.worker') do
  PlansWorker.perform_async
end
