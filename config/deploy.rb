require "bundler/capistrano"

load "config/recipes/base"
load "config/recipes/nginx"
load "config/recipes/unicorn"
load "config/recipes/postgresql"
load "config/recipes/redis"
load "config/recipes/rbenv"
load "config/recipes/check"

require "sidekiq/capistrano"
load    "config/recipes/clockwork"

server "74.207.231.153", :web, :app, :db, primary: true

set :user, "deployer"
set :application, "codegears"
set :deploy_to, "/home/#{user}/apps/#{application}"
set :deploy_via, :remote_cache
set :use_sudo, false

set :scm, "git"
set :repository, "git@bitbucket.org:codegears/server-api.git"
set :branch, "master"

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

after "deploy", "deploy:cleanup" # keep only the last 5 releases