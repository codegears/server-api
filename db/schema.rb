# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20121009213744) do

  create_table "api_keys", :force => true do |t|
    t.string   "access_token", :default => "", :null => false
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "api_keys", ["access_token"], :name => "index_api_keys_on_access_token", :unique => true

  create_table "app_instances", :force => true do |t|
    t.integer  "plan_id",        :default => 0, :null => false
    t.integer  "data_center_id", :default => 0, :null => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "availables", :force => true do |t|
    t.integer  "plan_id",        :default => 0, :null => false
    t.integer  "data_center_id", :default => 0, :null => false
    t.integer  "count",          :default => 0, :null => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "data_centers", :force => true do |t|
    t.integer  "uid",        :default => 0,  :null => false
    t.string   "location",   :default => "", :null => false
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  add_index "data_centers", ["uid"], :name => "index_data_centers_on_uid", :unique => true

  create_table "db_instances", :force => true do |t|
    t.integer  "api_key_id",     :default => 0, :null => false
    t.integer  "plan_id",        :default => 0, :null => false
    t.integer  "data_center_id", :default => 0, :null => false
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
  end

  create_table "plans", :force => true do |t|
    t.integer  "uid",                                       :default => 0,  :null => false
    t.string   "label",                                     :default => "", :null => false
    t.integer  "ram",                                       :default => 0,  :null => false
    t.integer  "disk",                                      :default => 0,  :null => false
    t.integer  "xfer",                                      :default => 0,  :null => false
    t.decimal  "price",      :precision => 10, :scale => 2,                 :null => false
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
  end

  add_index "plans", ["uid"], :name => "index_plans_on_uid", :unique => true

  add_foreign_key "app_instances", "data_centers", :name => "app_instances_data_center_id_fk", :dependent => :delete
  add_foreign_key "app_instances", "plans", :name => "app_instances_plan_id_fk", :dependent => :delete

  add_foreign_key "availables", "data_centers", :name => "availables_data_center_id_fk", :dependent => :delete
  add_foreign_key "availables", "plans", :name => "availables_plan_id_fk", :dependent => :delete

  add_foreign_key "db_instances", "api_keys", :name => "db_instances_api_key_id_fk", :dependent => :delete
  add_foreign_key "db_instances", "data_centers", :name => "db_instances_data_center_id_fk", :dependent => :delete
  add_foreign_key "db_instances", "plans", :name => "db_instances_plan_id_fk", :dependent => :delete

end
