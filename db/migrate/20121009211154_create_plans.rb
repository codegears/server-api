class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.integer :uid,   null: false, default: 0
      t.string  :label, null: false, default: ''
      t.integer :ram,   null: false, default: 0
      t.integer :disk,  null: false, default: 0
      t.integer :xfer,  null: false, default: 0
      t.decimal :price, null: false, dafault: 0.00, precision: 10, scale: 2

      t.timestamps
    end

    add_index :plans, :uid, unique: true
  end
end
