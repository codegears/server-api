class CreateAppInstances < ActiveRecord::Migration
  def change
    create_table :app_instances do |t|
      t.integer :plan_id,        null: false, default: 0
      t.integer :data_center_id, null: false, default: 0

      t.timestamps
    end

    add_foreign_key :app_instances, :plans,        dependent: :delete
    add_foreign_key :app_instances, :data_centers, dependent: :delete
  end
end
