class CreateAvailables < ActiveRecord::Migration
  def change
    create_table :availables do |t|
      t.integer :plan_id,        null: false, default: 0
      t.integer :data_center_id, null: false, default: 0
      t.integer :count,          null: false, default: 0

      t.timestamps
    end

    add_foreign_key :availables, :plans,        dependent: :delete
    add_foreign_key :availables, :data_centers, dependent: :delete
  end
end
