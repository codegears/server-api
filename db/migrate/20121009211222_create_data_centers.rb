class CreateDataCenters < ActiveRecord::Migration
  def change
    create_table :data_centers do |t|
      t.integer :uid,      null: false, default: 0
      t.string  :location, null: false, default: ''

      t.timestamps
    end

    add_index :data_centers, :uid, unique: true
  end
end
