class CreateDbInstances < ActiveRecord::Migration
  def change
    create_table :db_instances do |t|
      t.integer :api_key_id,     null: false, default: 0
      t.integer :plan_id,        null: false, default: 0
      t.integer :data_center_id, null: false, default: 0

      t.timestamps
    end

    add_foreign_key :db_instances, :api_keys,     dependent: :delete
    add_foreign_key :db_instances, :plans,        dependent: :delete
    add_foreign_key :db_instances, :data_centers, dependent: :delete
  end
end
