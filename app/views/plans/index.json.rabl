unless @data_center.nil?
  object @data_center
  attributes :location
  child :availables do
    attributes :count
    child :plan do
      attributes :label, :price, :ram, :disk, :xfer
    end
  end
else
  node(:error) do
    { 'message' => 'Data center not found',
      'hint'    => "'#{URI.join(root_url, data_centers_path)}' for available centers"
    }
  end
end