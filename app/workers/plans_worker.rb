class PlansWorker
  include Sidekiq::Worker

  def perform
    begin
      remote_data_centers = L.avail.datacenters || []
      remote_plans        = L.avail.linodeplans || []
    rescue
    end

    # update data centers
    remote_data_centers.each do |e|
      e_hash = {
          uid:      e.datacenterid,
          location: e.location
      }

      data_center = DataCenter.find_by_uid e.datacenterid
      data_center.nil? ? data_center = DataCenter.create(e_hash) : data_center.update_attributes(e_hash)
    end

    # update plans
    remote_plans.each do |e|
      Plan.transaction do
        e_hash = {
            price:  e.price,
            ram:    e.ram,
            xfer:   e.xfer,
            uid:    e.planid,
            label:  e.label
        }

        plan = Plan.find_by_uid e.planid
        plan.nil? ? plan = Plan.create(e_hash) : plan.update_attributes(e_hash)

        # update available plans in data centers
        e.avail.each do |ea|
          data_center = DataCenter.find_by_uid ea.first
          unless data_center.nil?
            ea_hash = {
                plan_id:        plan.id,
                data_center_id: data_center.id,
                count:          ea.last
            }

            avail = Available.where(plan_id: plan.id, data_center_id: data_center.id).first
            avail.nil? ? Available.create(ea_hash) : avail.update_attributes(ea_hash)
          end
        end
      end
    end
  end
end