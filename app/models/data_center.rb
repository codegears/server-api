class DataCenter < ActiveRecord::Base
  has_many :db_instances
  has_many :app_instances
  has_many :availables
  has_many :plans, through: :availables

  attr_accessible :uid, :location

  default_scope order('updated_at DESC')
end
