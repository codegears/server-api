class Plan < ActiveRecord::Base
  has_many :db_instances
  has_many :app_instances
  has_many :availables
  has_many :data_centers, through: :availables

  attr_accessible :uid, :price, :ram, :xfer, :label

  def label=(value)
    new_value = value.scan(/\A(Linode )([A-z0-9]+)\z/).first.last
    new_value += 'MB' if /GB/.match(new_value).nil?
    self[:label] = new_value
  end
end
