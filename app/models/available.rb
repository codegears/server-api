class Available < ActiveRecord::Base
  belongs_to :plan
  belongs_to :data_center

  attr_accessible :plan_id, :data_center_id, :count
end
