class AppInstance < ActiveRecord::Base
  belongs_to :api_key
  belongs_to :plan
  belongs_to :data_center
end
