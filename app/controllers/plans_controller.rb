class PlansController < ApplicationController
  def index
    @data_center = DataCenter.where(:id => params[:data_center_id]).includes(:availables => :plan).first
    render :index
  end
end
