class DataCentersController < ApplicationController
  def index
    @data_centers = DataCenter.all
    render :index
  end
end
